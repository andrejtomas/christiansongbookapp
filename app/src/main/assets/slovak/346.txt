Verse 1
Z krajiny, kde byt časný mám, 
do slávnej Otčiny sa uberám. 
Lístok je už zakúpený, 
Golgotou bol už raz draho platený.

Chorus
Za hviezdnatým morom v diaľke 
žiari môj dom, v ktorom budem 
bývať s mojím Spasiteľom. 
Otvorené brány z perál  
už teraz zriem: 
hľa, zlaté mesto, Jeruzalem.

Verse 2
Prekvapením omámený zastanem 
v šťastnom kruhu spasených. 
Nádherný lesk ma oblaží, 
veniec keď podá mi Ježiš najdrahší.

Verse 3
Nebeský kraj je samý jas, 
túžobne vierou vidím mesto krás. 
Neviem ten deň, iba to viem, 
na loďke života tam že pristanem.
