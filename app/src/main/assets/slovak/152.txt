Verse 1
Či chceš väzby hriechu  
byť sprostený? 
Moc v Krvi je! 
Moc v Krvi je! 
Poď, mier v dušu bude 
ti vložený, 
k očisteniu moc 
v Krvi je!

Chorus
Leží moc, moc, 
divotvorná moc 
v tej Krvi, v tej Krvi. 
Leží moc, moc, 
víťaziaca moc
v tej Krvi 
Spasiteľa len!

Verse 2
Chceš vymanenie 
z náruživosti? 
Moc v Krvi je! 
Moc v Krvi je! 
Krv z Golgoty 
celkom ťa očistí, 
víťazstvo v drahej 
Krvi je!

Verse 3
Chcel bys´ belším byť  nad 
bielučký sneh? 
Moc v Krvi je! 
Moc v Krvi je! 
Tá Krv všetky škvrny 
vezme i hriech, 
čistota plná v 
Krvi je! 

Verse 4
Chcel bys´ Kristu slúžiť 
tu oddane? 
Moc v Krvi je! 
Moc v Krvi je! 
Pána lásky sláviť 
neprestajne, 
denne moc k tomu 
v Krvi je!
