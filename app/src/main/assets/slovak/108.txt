Verse 1
Ježišovi svoj život daj, 
pokiaľ si plný síl.  
Uletia roky, skoro sa  
pred Trón Boží postavíš.

Chorus
Krásny je život s Ježišom, 
prečo bys´, brat môj, lkal? 
Plnosť a hojnosť dáva On, 
rýchlo sa Mu odovzdaj!

Verse 2
Po šťastí túžiš, pokoji, 
v srdci len pustota .... 
Pán na sto percent spokojí, 
radostnú ti vieru dá.

Verse 3
Svet tento na čas pobaví, 
drahú mu platíš daň. 
Pán Ježiš hriechu pozbaví, 
vybuduje v srdci chrám.

Verse 4
Ó, rozhodni sa Jemu žiť, 
nezvädne žitia kvet. 
Blažený, šťastný môžeš byť, 
ponáhľaj sa k Nemu hneď!


