Verse 1
S Ježišom kráčam 
iste to viem 
S ním Neba slávu 
tú prežívať smiem.
Božie som dieťa 
kúpil ma On
Krv zmyla hriechy 
pokoj mám v ňom.

Chorus
/: Tebe buď chvála 
Ježišu môj,
istotu spásy dal 
mi kríž Tvoj :/			

Verse 2
Chcem sa mu podať 
seba sa vzdať
Slobodu v náručí 
Jeho len mať
Túžim vždy vyznať 
On je môj Pán,
Každý deň lásku, 
radosť v ňom mať.

