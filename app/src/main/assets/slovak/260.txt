Verse 1
/:Rozhodol som sa 
ísť za Ježišom:/ 
už nikdy späť, už nikdy späť.

Verse 2
/:Svet leží za mnou 
a kríž predo mnou:/  
už nikdy späť, už nikdy späť.

Verse 3
/:Aj sám keď pôjdem,  
nechcem sa vrátiť:/ 
už nikdy späť, už nikdy späť.

Verse 4
/:Poď, aj ty so mnou,  
poď za Ježišom:/ 
už nikdy späť, už nikdy späť.