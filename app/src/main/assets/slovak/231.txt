Verse 1
Pane, som tak veľmi rád,
život s Tebou som už skúsil.
Tak Ti spievam pieseň chvál,
poznať Teba vždy som túžil.

Verse 2
Z nebies zostúpil si k nám 
na túto zem,
tu na kríži musels´ mrieť 
za môj hriech.
Kríž pred hrobom v krvi stál,
z hrobu otec si ťa vzal,
tak ti spievam pieseň chvál.