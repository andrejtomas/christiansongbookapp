Verse 1
Nech znie Bohu sláva, 
dnes každý Ho chváľ,
že v svojom nám 
Synovi spasenie dal.
Keď hriech v priepasť 
strašnú ma už strhával,
tu Ježiš mi k záchrane ruku podal.

Chorus
Jeho chváľ, Jeho chváľ
celým srdcom Ho chváľ.
Jeho chváľ, Jeho chváľ
Ježiš Kristus je Kráľ. 
Nech znie Bohu sláva,
dnes každý Ho chváľ,
že v svojom nám Synovi 
spasenie dal.

Verse 2
Tak vzdaj Bohu slávu 
za Golgoty vrch,
kde na kríži zaplatil 
Ježiš tvoj dlh.
Pán zná tvoje bolesti,
každý tvoj žiaľ
a túži bys´ srdce 
Mu celé tiež dal.

Verse 3
Keď znie Bohu sláva, 
ty nemožeš spať,
On volá ťa k životu 
a má ťa rád.
Ked znie Bohu sláva, 
tak i ja plesám
moj Spasiteľ žije, 
moj Priateľ, moj Pán.


