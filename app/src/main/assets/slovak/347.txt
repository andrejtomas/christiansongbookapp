Verse 1
Z neistoty vyvedený,
vyvolenia diel mám 
zo tmy k svetlu privedený,
na česť Bohu spievam.
Z Jeho slova prúd istoty 
mocne pre mňa plynie,
koho Ježiš raz vyvolil, 
zahynúť nemôže.

Chorus
Duchom síce slúžim Bohu,
ale telom klesám.
On uznáva moju snahu 
pozdvihuje ma sám.
Duša bojom unavena
v Ňom si odpočinie,
koho Ježiš raz vyvolil,
zahynúť nemôže. 

Verse 2
V Kristu moja spravedlnosť
ustavične zkvetá.
Šťastnejší som nad boháča
i nad kráľov sveta.
Svet zahynie, hviezdy zhasnú,
nebe sa rozplynie,
koho Ježiš raz vyvolil,
zahynúť nemôže.