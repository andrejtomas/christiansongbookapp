Verse 1
Vzdávame Ti chválu,
česť a slávu,
pozdvihujúc ruky svoje,
K Tvojmu menu Svätému.

Verse 2
Lebo, Ty si Pán, 
veľké veci robíš nám,
nikto nie je ako Ty,
nikto nie je ako Ty.

Verse 3
Tebe Bože chvála,
česť a sláva,
Moje túžby k tebe letia,
Tvojmu menu Svätému.

Verse 4
Lebo, Ty si Pán, 
veľké veci robíš nám,
nikto nie je ako Ty,
nikto nie je ako Ty.