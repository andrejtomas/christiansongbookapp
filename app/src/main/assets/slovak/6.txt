Verse 1
Aké šťastie, Ježiša mať 
a s istotou smieť povedať:
Bôh mi všetko s Ním daroval, 
bych tu blaho, v nebi vlasť mal.

Chorus
Vlasť ty večná, vlasť ty moja, 
sídlo blaha a pokoja!
Môj záujem to jediný, 
do tvojej skôr prísť tíšiny.

Verse 2
Zasnúbil si ma na večnosť 
a hľa, jaká to blaženosť!
On ma nikdy neopustí, 
chcem Mu veriť a byť istý.

Verse 3
Tak úzko sa so mnou spojil, 
nikto by nás nerozdvojil.
Preto, duša plesá v slasti 
a túži po nebies vlasti.

Verse 4
Diabol, ktorý raz otročil, 
v tom záujme, aby zničil;
vždy odchádza a sa zlostí, 
nepraje mi tej milosti.

