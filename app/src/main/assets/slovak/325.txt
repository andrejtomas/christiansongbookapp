Verse 1
V živote som pravdu hľadal sám, 
to prázdno v duši neuhasí  
sveta lesk a klam. 
Žiadna útecha z ľudskej vedy,  
z múdrych slov, no ja som vedel,  
že má viacej Slova pre mňa Boh. 

Chorus
V tom zaznel Orla krik: 
"Poď z tade, dieťa milé, 
pod krídla moje choď, 
do Slova vovediem ťa. 
Naučím lietať ťa, 
však len krídla rozprestri." 
Naplnil blahom ma 
Božieho Orla krik.

Verse 2
Predtým som v nevernom svete žil, 
nič iba púšť, hoc hľadal som  
Teba zo všetkých síl. 
Len čerstvé Slovo vie  
uhasiť duše smäd, 
keď len predivnú milosť viacej 
poznať by som smel.
