Verse 1
Prišiel ku mne, prišiel ku mne.
Keď som nemohol ísť, kde je On,
prišiel ku mne.
Preto zomrel na Golgote,
keď som nemohol ísť, kde je On,
prišiel ku mne.