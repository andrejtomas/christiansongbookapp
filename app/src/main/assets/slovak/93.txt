Verse 1
Istotu vzácnu v srdci už mám:
spasil ma Ježiš, Boh môj a Pán!
Bremeno hriechov z duše mi sňal,
dedičom Božím tak som sa stal.

Chorus
Zazvučí chvály radostný tón,
vznáša  sa vrúcne pred Boží trón:
Sláva Ti, Pane, spásu že mám,
srdce a dušu v obeť Ti dám!

Verse 2
Aká to sláva, Pánom ho zvať,
do služieb Jeho navždy sa dať!
Priniesol spásu pre celú Zem,
pravdu tú ľuďom zvestovať chcem.

Verse 3
Oddaný Jemu, pokoj už mám,
srdce mi zmenil v svätý svoj chrám.
V predivnej sláve zjaví sa v Ňom,
vo všetkom všetkým je mi tu On.


