Verse 1
Ján bol v ten Deň Pánov
v Duchu Kristovom,
keď počul on Hlas z neba,
ktorý mu tam riekol:
"Ja som Alfa i Omega,
ten Počiatok i Koniec,
hľaď, Ja žijem už naveky."

Chorus
/: "Hľaď, Ja žijem,
hľaď, Ja žijem,
hľaď, Ja žijem už naveky." :/

Verse 2
Sedem zlatých svietnikov,
tam stojí živý Pán.
Má oči ako plameň
a meč vychádza z úst.
Vypovedal živé Slovo
ten Jeho Hlas mnohých vôd,
"Hľaď, Ja žijem už naveky."
