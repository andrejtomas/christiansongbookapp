Verse 1
Priateľ, idem do vlasti, poď s nami,
v Jeho ruke sme šťastný,
chceš isť tiež s námi?
Čo svet tebe ponúka je len lesk,
ver, Ježiš mrel za teba,
je tvoj záchranca.

Chorus
Tam je krásne, tak prekrásne,
chceš ísť s nami, ideme tam.
Tam je krásne, tak prekrásne,
tam pri Spasiteľovi je tak prekrásne.

Verse 2
Zlož k Nemu svoje hriechy hneď všetky,
poď k Nemu tak aký si,
On ti odpustí.
Hoc je cesta tŕnistá, Ježiš chce,
vždy po tvojej strane ísť,
k cieľu ťa priviesť.

Verse 3
Hore zmení sa v radosť všetok žiaľ,
ó, krásna je tá večnosť
žiť s Pánom spolu.
Aby nik tam nechýbal, duša poď,
dnes uteč večnej smrti,
a príď k milosti.
