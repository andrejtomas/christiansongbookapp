Verse 1
"Neboj sa, neľakaj!" 
Tak znie Pánov hlas. 
Žiari mi na cestu ako hviezdy jas. 
Cez polnočné mraky mocne preniká 
sladké zasľúbenie,  
že On ma nezanechá.

Chorus
/:Nie, nezanechá, nie, nezanechá, 
On sľúbil, že so mnou bude, 
nikdy ma nezanechá.:/

Verse 2
Ako v horúčave zvädne krásny kvet, 
tak sklamú tie sľuby, ktoré dáva svet. 
Však nezvädne ruža moja Sáronská, 
môj vodca nesklame,  
nikdy ma nezanechá.

Verse 3
Ešte budú mieriť šípy v moju hruď, 
ale Pán mi šepká:  
"Dobrej mysli buď!" 
Ako s piesňou vtáčik radostne lieta, 
tak mám v srdci radosť, 
že On ma nezanechá.

