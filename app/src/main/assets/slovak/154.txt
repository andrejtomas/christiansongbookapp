Verse 1
Môj Ježiš, si spásou, 
Pán nie je nik ako Ty.
Chcem v každý čas, 
tak ako dnes, 
Teba chváliť, Pán môj, Kráľ.
Môj Majster, so štítom, 
záchranou, si pokoj môj,
všetko čo viem, 
a všetko čo mám,
Tebe dávam, Pán môj, rád.

Verse 2
Sám si Boh môj, 
celá zem nech vidí,
že sláva a moc, 
majestát Ti patrí.
Hory pred trónom 
sa skláňajú
a Ty sám vládneš slávne.
Spievam Ti rád, 
ved´si Pán môj a Kráľ.
Naveky chcem stát, 
Teba stále mať rád.
Nič nie je viac ako 
láska, čo dal si nám.