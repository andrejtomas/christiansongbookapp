Verse 1
O mojom Pánu viac chcem znať, 
viac Jeho Pravdu zvestovať, 
viac Jeho krásy vidieť chcem, 
veď, že je láska, dobre viem.

Chorus
/:Viac o mojom Pánu,:/ 
viac z Jeho krásy vidieť chcem, 
veď, že je láska, dobre viem.

Verse 2
O mojom Pánu učte ma, 
čo pre mňa dobré je, On zná, 
Duch Jeho za noci i dňa šepce mi: 
"Ježiš ľúbi ma."

Verse 3
O mojom Pánu viac chcem znať, 
z Biblie svätej na stokrát, 
čo pre mňa drahý má ten Pán, 
za mňa čo kríž On niesol Sám.

Verse 4
O mojom Pánu viac znať mám, 
pre Jeho dielo rád viac dám, 
Jeho panstvo šíriť ja chcem, 
Knieža je mieru, dobre viem.
