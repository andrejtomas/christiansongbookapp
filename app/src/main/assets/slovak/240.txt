Verse 1
Pokrm prihotovil Kráľ, 
zdarma všetkých povolal, 
nieto v Jeho dome 
núdze ani hlad. 
Išiel na Golgotu mrieť, 
telo obetoval, chlieb,  
abys´ vierou nové víno 
mohol brať.

Chorus
K stolu Spasiteľ volá: 
"Rýchlo poď!" 
Miesto zaujmi 
a čerpaj z živých vôd. 
Slovom Božím vždy sa živ, 
budeš plný šťastia, síl, 
Pán ťa zasľúbením 
večným obdaril. 

Verse 2
Večne k nebu pozerám, 
že mne, najmenšiemu, Pán 
svoju nepremennú 
milosť daroval. 
Duchom Svätým vedie vpred,  
z lásky nado mnou chce bdieť, 
spievam ustavične Jemu  
pieseň chvál.

Verse 3
Pán Ťa volá, dúfaj, ver, 
Jeho príjmi, Bohu ver, 
pod kríž pokorne  zlož 
ťarchu svojich bied. 
Žitím kráčaj iba s Ním, 
verne Božiu vôľu čiň, 
raz sa oslávený v nebi  
budeš skvieť.


