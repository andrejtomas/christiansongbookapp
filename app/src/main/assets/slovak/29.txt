Verse 1
Boh zmocňuje tých, 
čo v Neho dúfajú 
a za Kristom ticho 
s krížom kráčajú. 
Tým On zvláštne 
dary Ducha zasľúbil, 
skrze ktoré Jeho 
ľud vždy zvíťazil.

Chorus
/:Víťazstvo:/, milé, 
slávne víťazstvo, 
/:víťazstvo:/ skrze Krv Jeho. 
Pán ide pred nami, 
vedie chodníkami, 
vždy s Ním zvíťazíme 
skrze Krv Jeho.

Verse 2
V boji s vernými 
do konca vytrvaj, 
aj keď diabol zúri, 
však sa nepoddaj! 
Pán je s tebou v boji, 
vždy len choď za Ním, 
v boji dá víťazstvo, 
v nebi budeš s Ním.

Verse 3
Po skončení boja 
koruna kynie, 
zvíťaziť dá ti Pán, 
hoc telo zhynie. 
V sile Ducha predivnú 
moc obdržíš, 
spasen budeš, iným 
k spáse poslúžiš.
