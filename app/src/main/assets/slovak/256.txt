Verse 1
Radosť mám veľkú, 
že som spasený, 
že Pán Ježiš ma vykúpil. 
V práci, v učení 
a v každom čase 
Jeho ruka ma vedie.

Chorus
Kto si spasený, 
raduj sa so mnou, 
spievaj piesne oslavné. 
O Ježišovi, o Jeho láske, 
o milosti nám danej.

Verse 2
Radosť mám veľkú, 
že som spasený, 
že Pán Ježiš ma vykúpil. 
Ježišova Krv 
obmyla aj mňa, 
dietkom Božím som sa stal.

Verse 3
Poď k Ježišovi, 
kým je ešte čas, 
kým trvá ešte milosť.  
Daj sa Jemu viesť, 
život svoj Mu 
zver, radosť naplní ti hruď.
