Verse 1
Chcem ti vyrozprávať, 
čím je pre mňa Ježiš. 
Od tej chvíle, keď sa 
Pánom mojím stal, 
celkom uvoľnil 
môj zotročený život. 
Čo svet nemôže dať, 
On mi hojne dal.

Chorus
Nikto nemiluje ako Ježiš, 
nikto nemôže tak verným byť. 
On ma vyslobodil 
z jarma vín a hriechov, 
teraz môžem šťastný žiť.

Verse 2
Než ma našiel, 
bol som podmanený hriechom, 
v srdci mojom vládla  
bieda, pustota. 
Vzal ma Spasiteľ 
na milostivé dlane, 
slávne preniesol 
na cestu života.

Verse 3
Deň za dňom je
Ježiš mojej duši bližšie. 
Deň za dňom je drahší  
Jeho ľúby hlas. 
Radujem sa,  
že raz úplne s Ním splyniem,  
až Ho uvidím v tej 
ríši nebies krás.

