Verse 1
Či si vykročil k nebeským horám, 
či si za chrbát zahodil svet? 
Či už Svätý Duch srdce ti plní, 
či v ňom láska je kvitnúci kvet? 
Či sa po ceste života brodíš 
plný nevery, trápenia, rán? 
Neplač, neostaň stáť, 
hore k nebesiam hľaď! 
Vládne, kraľuje Pán.

Chorus
Áno, kraľuje Pán, On národ svoj 
nezanechá, keď príde bôľ, skúška  
i nevery búrka, On vezme ťa na 
ramená. Áno, kraľuje Pán, zlosť 
diablova neškodí nám. 
Veď zasľúbenie bude naplnené. 
Vládne, kraľuje Pán!

Verse 2
Ak ťa únava na duši tlačí, 
potu kropaje na čele máš. 
Vidíš ako svet útrapy množí, 
sily ubúda, neodoláš. 
Vtedy komôrku vyhľadaj tichú, 
s Božím Baránkom zotrvaj tam. 
Jemu starosti zver, 
príjme, vyslyší, ver! 
Pozri: kraľuje Pán!

Verse 3
Keď zem opustil Spasiteľ drahý, 
svojim prisľúbil: "Vrátim sa späť!" 
Ako odísť Ho videli verní, 
Jeho príchod tak budú raz zrieť. 
Preto neboj sa, nekráčaj smutný, 
znášaj ústrky zo všetkých strán! 
Pán Boh zhovieva, viem, 
skoro príde ten deň. 
Vládne, kraľuje Pán!

