Verse 1
Egypte, dobrú noc, 
zajatie skončilo.
Leto milostivé, 
sloboda a sláva
tam nás očakáva 
za Červeným morom,
za púštou ďalekou. 
Egypte, dobrú noc.

Verse 2
Egypte, dobrú noc, 
my už odchádzame
k nebeským končinám 
k jordánskym dolinám,
v chlad jerišských paliem, 
kde ruže saronské
sladko rozváňajú. 
Egypte, dobrú noc.

Verse 3
Egypte, dobrú noc, 
Jeruzalem čaká,
brány otvorené,
hradby ovenčené,
zaviali prápory, 
spevy zazvučali.
Vítajú nás doma. 
Egypte, dobrú noc