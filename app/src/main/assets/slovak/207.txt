Verse 1
O, tá láska Tvoja Pane,
silná v moci nádhernej,
a jak morské valy pevná,
ó, rozlej sa v sile ctnej.
Nechže moje srdce plní
tej ľúbosti živý zdroj,
nesie tam kde bronie sláva,
vznesie v Raj nebeský môj.

Verse 2
Ó, tá láska Tvoja Pane,
je kráľovským zdrojom nám,
ľúbiť všetkých ten je vstave,
kto prijal ju v srdca chrám.
Tys' ľúbosťou tou, ó Jezu,
čo tak oblažila nás,
prelials' svoju krv na kríži,
veleben' buď v každý čas.

Verse 3
Ó, tá láska Tvoja, Pane,
nik neľúbi tak jak Ty !
Žriedlom ona lásky večnej,
spočinutím sladkým vždy.
O, tá láska Tvoja Pane,
nebom v zemskom žití je,
tam v tie nesie ona stány,
kde česť, chvála večne znie.