Verse 1
Včera, dnes i naveky 
náš Pán ten istý je, 
nech táto víťazná zvesť 
celým svetom znie. 
Jeho láska zachraňuje,  
uzdravuje svet, 
teší smutných, tíši búrky, 
buď Mu chvála, česť!

Chorus
Včera, dnes i na veky 
náš Pán ten istý je, 
svet sa mení, Ježiš nikdy, 
Jemu patrí česť! 
/:Jeho Menu česť!:/ 
Svet sa mení, Ježiš nikdy! 
Buď Mu sláva, česť!

Verse 2
Priateľ hriešnych hľadá teba, 
márnotratný syn, 
ó, poď k Nemu, On Ťa zbaví 
pút a ťarchy vín. 
Neodsúdi, povie Ti však, 
choď a nehreš viac! 
Odpustí hriech, poteší ťa 
jak za dávnych čias.

Verse 3
Ježiš často uzdravoval 
biednych, nemocných, 
utíši aj naše žiale, 
dvíha bezmocných. 
Dá i tebe silu, zdravie,  
k Nemu poď a ver! 
Sťa tej žene uzdravenej 
dá ti v srdce mier!

Verse 4
Ako vtedy s učeníkmi 
do Emauz On šiel, 
chodí s nami žitia cestou, 
sleduje náš cieľ! 
Skoro Ho už uvidíme 
v Jeho slávny deň! 
Tento Ježiš z neba príde 
ako odišiel.


