Verse 1
/:Môj, môj,:/ Ježiš je môj, 
keď som strápený, aj keď veselý, 
/:môj, môj,:/ Ježiš je môj, 
Ježiš je vždycky môj.

Verse 2
/:Mám, mám,:/ Ježiša mám, 
s Ním som blažený  
a v Ňom spasený, 
/:mám, mám,:/ Ježiša mám, 
Ježiša vždycky mám.

Verse 3
/:Sám, sám,:/ s Ježišom sám 
tak často bývam,  
keď sa modlievam. 
/:Sám, sám,:/ s Ježišom sám, 
s Ježišom často sám.

Verse 4
/:Chcem, chcem,:/ Ježiša chcem, 
s Ním chcem vždy chodiť,  
Jeho velebiť. 
/:Chcem, chcem,:/ Ježiša chcem, 
Ježiša vždycky chcem.