Verse 1
Pán Boh je láska,
dal nás vykúpiť,
Pán Boh je láska,
ľúbi i mňa.

Chorus
Tak volám ešte raz:
Pán Boh je láska !
Pán Boh je láska,
ľúbi i mňa.

Verse 2
Ležal som v putách,
hriecha strašného,
v putách, a niak',
nemohol von.

Verse 3
On Spasiteľa,
Ježiša poslal,
aby ma navždy,
uvoľnil z nich.

Verse 4
On dal ma pozvať,
Slovom milosti,
On Duchom Svätým,
pozvať ma dal.

Verse 5
Ó, lásko svätá,
Ty liečiš bolesť,
ó, lásko večná,
Ty tešíš žiaľ.


Verse 6
Teba chcem chváliť,
Teba velebiť,
Tebe česť vzdávať,
kým žijem len.

