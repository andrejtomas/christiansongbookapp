Verse 1
Ja pôjdem s Tebou, kam len chceš,
že milujem Ťa, dobre vieš;
a že Ty ľúbiš mňa, to znám,
o, hlas tej lásky v srdci mám.
Ó, pôjdem s Tebou, kam len chceš,
/:pôjdem, ak Ty ma povedieš.:/

Verse 2
Za Tebou túžby mojej let
nesie sa v nadhviezdny Tvoj svet,
kde kráľovský Svoj domov máš,
kde aj mne podiel odkladáš.
Oj, túžim, túžim, Kráľu môj,
/:v nádherný, slávny domov Tvoj!:/

Verse 3
Keď nemôžem tvár Tvoju zrieť,
bo opustil Si tento svet,
tak srdce svoje otváram,
v ňom Duchu Tvojmu miesto dám,
nech stavia Tebe chrám i trón:
/: tak uvedie ma na Sion.:/

Verse 4
Bárs biedny zemský prach som len,
spravedlnosť moja ako sen,
Ty rúcho biele i mne dáš
a okrás zlatých pre mňa máš!
To všetko vyslúžils' mi sám
/: tak verím dúfam, pospiecham.:/