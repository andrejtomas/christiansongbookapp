Verse 1
Viem, neoklame ma Pán, v Neho 
dúfať chcem. Viem, neoklame ma 
Pán, vždy On vzpruží ma. 
Aj keď hrozí búrky des mrak, 
víchor zdvíha sa, ja v Bohu nádej 
vždy mám, neoklame ma.

Verse 2
On je Priateľ môj, verný až k smrti. 
Podviesť môže svet, ale On nikdy. 
Aj keď hrozí búrky des mrak, 
víchor vzdúva sa, ja v Bohu nádej 
vždy mám, neoklame ma.