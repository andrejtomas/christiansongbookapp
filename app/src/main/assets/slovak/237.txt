Verse 1
Poď k Ježišovi, poď ešte dnes, 
svoje Mu srdce k obeti nes. 
On z Trónu Svojho,  
kde večný ples, 
tak sladko volá: "Poď."

Chorus
Sláva, sláva dušu ožiari, 
až pri Božom stanem oltári, 
kde svätosť našu hriech nezmarí, 
ó, Pane, idem už!

Verse 2
Poď, dieťa milé, ten sladký hlas 
i teba volá, poď, kým je čas! 
Kto zná, či zajtra ozve sa zas, 
ó, nemeškaj, len poď.

Verse 3
Poď, kto si koľvek, hľa, blízko Pán, 
za všetkých niesol bolesti rán. 
Všetkých nás volá  
zo všetkých strán, 
ó, čuj a ver a poď.
