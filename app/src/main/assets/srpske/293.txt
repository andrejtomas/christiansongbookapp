Verse 1
Pitam se šta ću reći kad se 
budemo gledali, oči u oči, ja i Ti.
Šta ću li tada reći?
Hoću li izdržati, kad se budemo 
gledali, oči u oči, ja i Ti?
Hoću li moći izdržati?

Verse 2
Ref:  Kad se budemo gledali oči u oči, 
kad se budemo sreli ja i Ti.
Seti se da sam samo čovek. 
Seti se da sam samo čovek,
a Bog da si Ti, a Bog da si Ti…

Verse 3
Kakav li ću biti kad se budemo 
gledali oči u oči, ja i Ti?
Kakav li ću onda biti?
Kako će to biti kad se budemo 
gledali oči u oči, ja i Ti?
Jedan čovek i njegov Bog.

