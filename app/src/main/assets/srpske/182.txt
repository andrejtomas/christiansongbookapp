Verse 1
Kako dobar nam je Isus, 
uzvišen vrh neba sad. 
S Ocem On nas sve izmiri, 
te se za nas moli rad. 
Ko bi mogo proceniti 
koliko mi gubimo, 
Njega kad ne prizivamo, 
srcem Ga ne ljubimo.

Verse 2
Kad dušmana moć nam preti, 
silne bure oko nas. 
Nemojmo se uplašiti, 
molitve dižimo glas. 
Tada videćemo kako Isus 
uz nas stoji sam. 
On će čuti naše molbe, 
izbavljenje daće nam.

Verse 3
Muče li nas brige ljute 
kasno rano svaki čas. 
Isus stalno nam pomaže, 
nikad ne ostavlja nas. 
Napustel' nas prijatelji 
u molitvi nam je lek, 
Car, sveštenik a i prorok 
biće nama Isus vek.
