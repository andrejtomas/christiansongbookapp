Verse 1
Stvoritelju neba, a i zemlje cele 
koji  život i meni poklanjaš.
Anđeli se raduju stvaralaštvu Tvom
i ljubavi kojoj kraja nema.

Chorus
Samo Ti o Bože, čuješ moje molitve
i misli koje srce samo zna.
Tebi Bože pevam najlepše pesme
i u pokornosti stojim tu i slavim Te.

Verse 3
Stvoritelju neba, 
a i zemlje cele,
ispred lica Tvog i 
najdublja tama nestaje.
Odkad sam te upoznao, 
život se promenio.
i sad nešto novo rađa se.