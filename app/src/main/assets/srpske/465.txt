Verse 1
Dal si umoran i sav u tuzi, 
kaži Isusu, kaži Isusu!
Nemaš prijatelja koj' te ljubi, 
kaži samo Isusu.

Chorus
Kaži Isusu, kaži Isusu, 
jer On je prijatelj tvoj!
Nemaš takvoga ni brata 
vernog kao što je Isus sam.

Verse 2
Teški oblak život kad pokriva, 
kaži Isusu, kaži Isusu!
I pomišljaš šta će biti sutra, 
kaži samo Isusu! 

Verse 3
Ako moraš suze da prolivaš, 
kaži Isusu, kaži Isusu!
On jedini svakoga razume, 
kaži samo Isusu! 

Verse 4
Kad se plašiš kada smrt dolazi, 
kaži Isusu, kaži Isusu!
Kad ti duša u večnost odlazi, 
kaži samo Isusu! 

