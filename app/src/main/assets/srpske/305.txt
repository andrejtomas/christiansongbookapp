Verse 1
Oče naš na nebu, jagnje Božije,
klanjamo se tebi: svet si svet si.
Celo nebo peva pesmu spašenih.
Daje slavu jagnjetu!

Chorus
Tvoja krv spasava nas. 
Tvoja krv oslobađa.
Kroz tvoju krv dolazimo 
pred tron svetog Oca.
Tvoja ljubav oprašta nam. 
Tvoja sila podiže nas.
Tvoja krv pere svaki moj greh. 2x

