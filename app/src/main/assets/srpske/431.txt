Verse 1
U divnoj haljini, večnoj domovini
u divnome kraju, gde noći nestaju.
Prekrasna je kruna za nas pripravljena,
Gospode Reč Tvoja vodi nas tam’.

Chorus
Krasan je dvor, krasan izvor,
krasan je put, koj' vodi tu.
Sve je divno i večito,
nema tam ništa što je prolazno.

Verse 2
U rajskome vrtu, radosno hodaću,
gde j' drvo života i divni plodovi.
Uživaćemo mi, svi koj' smo spaseni,
Gospode Reč Tvoja vodi nas tam'. 

Verse 3
Prekrasni plodovi i slatki izvori
teku sa prestola, pijmo iz njih sada!
U večnoj svetlosti, beskrajnoj radosti,
Gospode Reč Tvoja, vodi nas tam'. 

Verse 4
Prekrasan večni grad, svaki je tamo mlad.
Radosno uživa ko tamo prebiva!
Ne treba sunca sjaj, Isus je sunce sam,
Gospode Reč Tvoja vodi nas tam'.
