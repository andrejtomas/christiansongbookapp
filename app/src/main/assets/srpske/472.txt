Verse 1
Celu noć uzalud plovim 
ja sam ribar bez sreće.
Prazna mreža u čamcu leži, 
od mene i ribe beže.
Tamo na obali gleda me čovek 
i čujem kako me zove,
priđi bliže i mrežu zabaci, 
ovde su ribe on reče.

Chorus
Ribari ljudi, budite vi,
ribari ljudi sa mrežom ljubavi. /2x

Verse 3
Toliko riba ne videh do tada, 
da bude čudo još veće,
ljubavlju svojom on me privuče 
i noć zablista od sreće.
Ja ne znam kako se stvorio tu 
i ne znam zašto baš mene,
Isus Hrist Božji sin 
za druga da odabere. 

