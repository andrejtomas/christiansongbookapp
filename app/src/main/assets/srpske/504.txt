Verse 1
U predveče, svetlo nam’ biće, 
Ka slavi put, ko želi videće!
Putem u vodu, naći ćeš slobodu!
Kršten u to slavno Ime ISUS!
Mlad i star, od greha se pokaj, 
i Sveti Duh će ući tada, znaj!
Svetlo je nam to: da Bog i Hrist 
su jedno te isto!