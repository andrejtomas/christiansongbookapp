Verse 1
Pred tobom smo, o Gospode.
Svoj život tebi dajemo.
I donosimo dar na oltar tvoj,
dar hvale i slave tebi!

Chorus
Dolazimo hrabro pred tvoje lice.
Zbog milosti prihvataš nas.
Za život svoj prolio si krv jagnjeta.
Naš život je predan u ruke tvoje
i spremno ti dajemo sve.
Kad pred tvojim nogama se klanjamo.
To mesto uzviseno je Bože.

Verse 3
Na tom svetom tlu naše je srce ponizno.
Tvoja moć preuzima nas sve.
I nije nas strah jer Hriste Ti si tu.
Tvoja sveta krv pripremila je put.

