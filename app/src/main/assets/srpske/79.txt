Verse 1
Duh je Boga svevišnjeg u meni,
pomazanje da objavim, tu dobru vest. 2x
Siromašnima On me šalje, ovo je godina,
da isceli ranjene u srcu, ovo je dan,
da proglasi sužnjima slobodu, ovo je godina,
i izvede ih iz tame!

Chorus
Ovo je godina Božje milosti za nas!
Ovo je dan Hrista našeg Gospoda! 2x

Verse 3
Duh je Boga svevišnjeg u tebi,
pomazanje da objaviš, tu dobru vest. 2x
Utešiće žalosne, ovo je godina,
mesto pepela dati im krunu, ovo je dan,
izliće ulje radosti, ovo je godina,
mesto tuge slavljenje!

Chorus
Ovo je godina Božje milosti za nas!
Ovo je dan Hrista našeg Gospoda! 2x

