Verse 1
Uzmi sad mene Gospode moj
jer Ti si lončar, ja sam kal Tvoj.
Čini od mene šta želiš Ti,
predano stupam pred tron sveti.

Verse 2
Uzmi sad mene Gospode moj,
u svome ognju me oprobaj,
belji od snega želim biti,
u Tvojoj krvi me operi.

Verse 3
Uzmi sad mene Gospode moj,
upravljaj sa mnom na zemlji toj,
sila i slava Tvoja je znam
iscelit spasit' možeš Ti sam.

Verse 4
Uzmi sad mene Gospode moj,
nek vlast nadamnom ima Duh Tvoj.
Nek' vlada sa mnom da vide svi,
da sad u meni Isus živi.