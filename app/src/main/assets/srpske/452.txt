Verse 1
Velik je Bog i hvale dostojan, 
Njemu pevam pesmu novu!
On je moj Kralj, snaga i tvrđava, 
o Jahve Ti si uzvišen!

Chorus
Tebe slaviću, Tebi živeću, 
Tebi pevam pesmu novu!
Tebe slaviću, Tebi živeću, 
Tebi pevam pesmu novu, Bože moj!

Verse 2
Sa pesmom u srcu i hvalom na usnama 
Tebi Bože kličem radostan!
S gitarom i bubnjem uz zvuke slavljenja, 
pred Tobom plešem Bože moj! 

