Verse 1
Verujem ti Gospode da Sin 
si Boga živoga,
da iz mrtvih ti uskrsnu, 
da došao si da spasiš sve.
Duša ti moja veruje, 
da Ti si s nama zauvek,
sila me Tvoja ozdravlja, 
milost mi sve oprašta.

