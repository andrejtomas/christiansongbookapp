Verse 1
Šta oprat' može moj greh? 
Ništa nego krv Isusa.
Šta izlečit' može me? 
Ništa nego krv Isusa.

Chorus
O dragocena krv, 
koja greh pere sav,
za drugo ne znam ja, 
ništa nego krv Isusa.

Verse 3
Šta će greh moj očistit? 
Ništa nego krv Isusa.
Šta će mene spasiti, 
ništa nego krv Isusa. 

Verse 4
Nema žrtve za grehe, n
išta nego krv Isusa.
Dela neće spasit' te, 
ništa nego krv Isusa. 

Verse 5
Šta mi daje mir i nad? 
Ništa nego krv Isusa.
Šta će meni pomoć tad?
Ništa nego krv Isusa. 
