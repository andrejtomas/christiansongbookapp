Verse 1
Imam jednog nebeskog 
cara samo njega poznajem ja.
Njeg jedinog na zemlji ljubi
i slavi vek duša moja.

Chorus
Isus moj ponos moj mir. 
Isuse kličem Ti sad.
Ja imam nebeskog cara, 
Isuse samo Tebe.

Verse 3
Ja stupam sad rado u službu, 
tog cara što ljubi me.
S veseljem izvršavam volju, 
tog cara što daje mi sve.

Verse 4
Ja verujem Reči tog cara, 
po njoj upravljam život svoj.
Za tragom Njegovim ja idem, 
makar kroz sramotu i bol. 

Verse 5
Hoću da sve moje na svetu 
tom caru je podređeno
i željno ja čekam da dođem 
u Njegovo večno carstvo. 

