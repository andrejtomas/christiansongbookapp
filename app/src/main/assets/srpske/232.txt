Verse 1
Naš slavni lekar sad je tu, 
premili dragi Isus.
On utehu nam daje svu, 
naš spas je samo Isus.

Chorus
Slušaj peva neba poj, 
anđela to silni broj.
I ti srce moje poj: 
"Isus, Isus, Isus!"

Verse 3
Isplaćen je sad naš dug sav, 
a trpeo je Isus.
U nebo doće ko je prav, 
krunisaće ga Isus. 

Verse 4
Na krstu umre za me Ti, 
hvala Ti zato Hriste.
Verujem da mi opraštaš, 
i ja te ljubim Spase. 

Verse 5
Ostavio nas greha strah, 
to čini mili Isus.
Spas duše nađe zemni prah, 
kroz žrtvu našeg Spasa. 

Verse 6
Hajd' braćo da zapevamo nek' 
hvaljen bude Isus.
Hajd' sestre i vi sad amo, 
slavite Ime Isus. 

Verse 7
Kad dođe onaj velik' dan, 
gledaću Tebe Hriste.
I pevaću kroz večnost svu: 
"O, slavno Ime Isus."

