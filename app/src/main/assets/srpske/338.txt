Verse 1
Ti svetlost sveta si došao na zemlju,
U tami Svetlo je zasvetlilo.
Oči si moje otvorio da vidim,
Novi život nade sa Tobom.

Chorus
Tu sam da te slavim 
tebi da se klanjam,
Da kažem ti da Ti si Gospod moj.
Jer dostajan si slave 
dostajan si hvale,
Dostojan da Tebi klanjam se.

Verse 2
Kralju kraljeva večno 
vladaš sa neba.
Uzvišen na tronu sediš sam.
Svoj život si za mene ponizno dao,
Jer si mene silno voleo. 

Verse 3
Nikad neću znati koliko
na krstu si za mene platio. /2x


