Verse 1
Bog je među nama, 
Njemu se klanjajmo,
i u strahu pristupajmo.
Bog je sada s nama, 
Njemu se predajmo,
na kolena svi padajmo.
Ko Ga zna neka sva 
čuvstva srca svoga,
uzdigne do Boga.

Verse 2
Mi se odričemo sveta ovog slasti,
taštih želja svake časti.
Život, telo duša naše 
sposobnosti,
da budu u Tvojoj vlasti. 
Jedin' Ti nama si Gospod 
i Bog svima,
blažen ko Te prima.

Verse 3
Svemogući Bože, 
daj da svetlo Tvoje
padne i na lice moje.
Ko što nežno cveće 
lepo se razvija
jer mu svetlost sunca prija.
Tako svak' Tvoj nam zrak, 
neka grud zagreje,
srca led razbije.

Verse 4
Daj da jesmo blagi iskreni i verni,
prostodušni krotki smerni,
daj nam čisto srce 
a i ljubav pravu,
da vidimo Tvoju slavu.
Srce nam digni sam, 
da Tebi o Bože
svak' ugodit može.

