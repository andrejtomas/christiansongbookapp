Verse 1
Uzmi drago ime Isus, 
dete jada žalosti,
ono će ti dati radost, 
kud god ideš uzmi Ga.

Chorus
O kako dražesno, 
ime Isus presveto.
Nada je zemaljska a 
i radost nebeska.

Verse 2
Uzmi drago ime Isus 
kao štit od kušnja svih,
udahni Ga u molitvi, 
ono će ti dati mir. 

Verse 3
Klanjajmo se tom imenu, 
svoga Cara slavimo,
mi ćemo Ga krunisati
kad u nebo dođemo. 

