Verse 1
Došao je kao sluga, 
postao je sličan ljudima.
Stigao je poput izdanka. 
Prezren beše bez lepote,
Čovek bola, vičan patnjama, 
od kog svako lice zaklanja.
Ko je vređan, nije vređao. 
Ko je mučen nije pretio.

Chorus
On je spasitelj svima,
On je uteha pravednima.
Njegov sledimo glas dok
vodi nas putevima.
Kao jagnje na klanje ga odvedoše.
Za opačine sveta ga probodoše.
Radi našega mira, Njega osudiše

