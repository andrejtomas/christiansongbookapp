Chorus
A ja idem samo za Njim
Njegovu reč verno sledim
to samo Božja milost snagu daje mi.
Zašto si danas tužan ti kad Božja
reč nam govori, da svaku brigu,
problem Njemu predaš ti.

Verse 1
Jednom davno to je bilo
a ja nisam zaslužio
da moj Gospod trpi
muke za mene.
On Svojom žrtvom spasi me
i moje ime prozove
danas stojim ovde
i proslavljam Te.

Chorus
A ja idem samo za Njim
Njegovu reč verno sledim
to samo Božja milost snagu daje mi.
Zašto si danas tužan ti kad Božja
reč nam govori, da svaku brigu,
problem Njemu predaš ti.

Verse 2
Često mislim sve je bliže
naše izbavljenje stiže
život naš nek bude ugodan i svet.
Ti si izvor naše snage
Ti si svetlo usred tame
Tvoje ime za nas večni život je.

Chorus
A ja idem samo za Njim
Njegovu reč verno sledim
to samo Božja milost
snagu daje mi.
Zašto si danas tužan ti kad Božja
reč nam govori, da svaku brigu,
problem Njemu predaš ti.

