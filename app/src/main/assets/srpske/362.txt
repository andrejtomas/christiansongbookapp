Verse 1
Rodio se u Judeji 
Spasitelj i večni car,
umre za nas na Golgoti 
da otkloni greha kar.
Pri odlasku ocu svome, 
obećanje dao je
ponovo doći će.

Chorus
Slava, slava haleluja, 
slava slava haleluja, 
slava slava haleluja,
naš Gospod dolazi.

Verse 2
Proročanstva reči Božje, 
već se skoro zbiše sva,
malo samo da potraje 
do dolaska Gospoda.
Tada blažen dan nastaje 
i zauvek ostaje
taj dan već blizu je.

Verse 3
Pismo piše divnu slavu 
dolaska Gospodnjega,
opisuje zemlju novu, 
obećanje višnjega.
On dolazi u svoj slavi 
sa krunama na glavi,
pred njim ide pravda.

Verse 4
Vest spasenja trubi svuda, 
da je već kraj sveta tu,
dok na zemlji vel'ka čuda 
ne bivaju uzalud.
Svedoče nam znaci ovi, 
veseli su glasovi,
naš Gospod dolazi.


