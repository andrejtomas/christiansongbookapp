Verse 1
Živeh nekada u taštini, 
ne mareći što Bog učini.
Da za mene bi Njegova 
smrt na Golgoti.

Chorus
Božja milost za mene to bi,
oprost od greha On dade mi.
On mene otkupi od smrti na Golgoti.

Verse 2
Božjom Rečju poznah grehe sve,
drhtah pred Bogom za prestupe,
dok se najzad ja ne obratih ka Golgoti.

Verse 3
Sebe predajem Isusu sad,
u Njem imam ja blaženu nad,
sad mogu radosno pevati o Golgoti.

Verse 4
O ljubavi i o spasenju,
o milosti što se ne menja.
Spasitelj je dug moj platio na Golgoti.

