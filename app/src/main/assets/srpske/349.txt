Verse 1
Ništa nije na svetu dragoceno
osim poziva Gospoda Isusa.
Bog je nama svog proroka poslao,
I kroz njega nam je tajne otkrio.

Chorus
Tada čuh orla krik, 
Bog mi progovori,
ti si Moj i Ja sam te izabrao.
Svaki greh oprao, 
sve zaboravio,
i Svojom Nevestom 
sam te nazvao.

Verse 3
Kako da objasnim tu radost i mir,
i zahvalnost što je 
sad u srcu mom.
O, Jehova ti si moj Bog jedini,
nema nikog drugog kao što si ti. 

Verse 4
Drage volje otvaram 
dušu svoju,
jer moj Gospod želi 
sve da Mu predam.
Želi da me vodi u dom na nebu,
meni tu na svetu ništa ne treba. 

Verse 5
Gospode moj dragi živi u meni,
proslavljaj Se, budi 
uvek sa mnom tu.
Hvala Ti za poziv 
Tvojoj Nevesti,
neka Sveti Duh Tvoj 
uzme slavu svu. 


