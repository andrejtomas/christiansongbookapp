Verse 1
Dok se divim tvojoj svetosti, 
dok toplinu Tvoju osećam.
Dok u sjaju Tvog svetla 
sve sena postaje.
Dok radost ispunjava mi srce, 
dok se ruci Tvojoj prepuštam sav.
Dok u sjaju Tvog svetla 
sve sena postaje:
Ja slavim Te, da slavim te. 
Ja živim zbog tog da slavim Te.

