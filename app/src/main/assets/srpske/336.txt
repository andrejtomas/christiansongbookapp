Verse 1
Živi Gospodu jer novi čovek si!
Živi Gospodu, sve staro prošlo je.
Živi Gospodu sad sve je novo gle!
Rođen duhom Božjim novi čovek si!

Verse 2
Budi slobodan jer novi čovek si.
Budi slobodan, sve staro prošlo je.
Budi slobodan, sad sve je novo gle!
Rođen duhom Božjim novi čovek si!

Verse 3
Pevaj, raduj se jer novi čovek si.
Pevaj, raduj se, sve staro prošlo je.
Pevaj, raduj se, sad sve je novo gle!
Rođen duhom Božjim novi čovek si!

Verse 4
Služi Gospodu jer novi čovek si.
Služi Gospodu, sve staro prošlo je.
Služi Gospodu, sad sve je novo gle!
Rođen duhom Božjim novi čovek si!