Verse 1
Volim da razmišljam o zemlji, 
gde žive anđeli,
I sveto snežnoj belini, 
Gospodu pevati.

Chorus
Gde nema greha, gde nema greha,
Gde nema greha i neće biti. /2x

Verse 2
Volim da razmišljam o zemlji, 
gde vlada Bog ljubavi,
I lepi prolećni ljiljani, 
svetle bojom radosti.

Verse 3
Volim da razmišljam o zemlji, 
u kojoj putnici žive,
Prolivši svoju krv u ratu
i našli su venac pobede. 


Verse 4
Volim da razmišljam o zemlji, 
u kojoj je posle bitke zla,
Ulaz za mene otvoren 
i mir sam našao tu ja. 


