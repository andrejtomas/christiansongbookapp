package com.andrejtomas.hriscanskapesmarica;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Objects;

public class SongOpened extends AppCompatActivity {
    Toolbar toolbar;
    TextView textViewSong;
    TextView textView1;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_song_opened);


        toolbar = findViewById(R.id.backToolbar);
        setSupportActionBar(toolbar);

        // add back arrow to toolbar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        Intent intent = getIntent();
        ExampleSong exampleItm = intent.getParcelableExtra("Example item");

        String line1 = Objects.requireNonNull(exampleItm).getSongTitle();
        String line2 = exampleItm.getSongNumber();

        textView1 = findViewById(R.id.songTitleOpened);
        textView1.setText(line1);
        TextView textView2 = findViewById(R.id.songNumOpened);
        textView2.setText(line2);
        textViewSong = findViewById(R.id.songLyrics);
        textViewSong.setMovementMethod(new ScrollingMovementMethod());
        loadSong(line2, textViewSong, MainActivity.currentItem);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.opened_menu, menu);

        final MenuItem copyButton = menu.findItem(R.id.copyText);
        copyButton.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                copyToClipBoard(textViewSong, textView1);
                return true;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            setCurrentList();
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setCurrentList();
    }

    private void setCurrentList() {
        if (MainActivity.currentItem == 0)
            MainActivity.navigationView.getMenu().getItem(0).setChecked(true);
        else if (MainActivity.currentItem == 1) {
            MainActivity.navigationView.getMenu().getItem(1).setChecked(true);
        } else if (MainActivity.currentItem == 2) {
            MainActivity.navigationView.getMenu().getItem(2).setChecked(true);
        }
    }

    private void loadSong(String num, TextView textViewSong, int numFromMain) {
        BufferedReader reader = null;
        try {
            String lang = "";
            if (numFromMain == 0){
                lang = "srpske/";
            }
            else if (numFromMain == 1) {
                lang = "slovak/";
            }
            else if (numFromMain == 2) {
                lang = "hrvatske/";
            }

            String text;
            StringBuilder sb = new StringBuilder();

            reader = new BufferedReader(
                    new InputStreamReader(getAssets().open(lang + num + ".txt")));
            while ((text = reader.readLine()) != null) {
                sb.append(text).append("\n");
                textViewSong.setText(sb.toString());
            }
        } catch (IOException e) {
            Toast.makeText(getApplicationContext(), num, Toast.LENGTH_SHORT).show();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    //log the exception
                }
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void copyToClipBoard(TextView songText, TextView songTitle) {
        ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        String stringBuilder = songTitle.getText().toString() + "\n\n" +
                songText.getText().toString();
        ClipData clip = ClipData.newPlainText(
                "Kopiranje teksta",
                stringBuilder);
        Objects.requireNonNull(clipboard).setPrimaryClip(clip);
        Toast.makeText(SongOpened.this, "Kopirano!", Toast.LENGTH_SHORT).show();
    }


}
