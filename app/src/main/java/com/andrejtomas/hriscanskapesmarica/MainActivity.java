package com.andrejtomas.hriscanskapesmarica;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.google.android.material.navigation.NavigationView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private static final String EMAIL = "crkvakovacica@gmail.com";
    private static final String WEBSITE = "https://www.biblijadanas.org";
    private static final String UPDATE = "https://play.google.com/store/apps/details?id=com.andrejtomas.hriscanskapesmarica";
    public static int currentItem = 0;

    DrawerLayout drawerLayout;
    ActionBarDrawerToggle actionBarDrawerToggle;
    Toolbar mainToolbar;
    public static NavigationView navigationView;
    ArrayList<ExampleSong> serbianList;
    ArrayList<ExampleSong> slovakList;
    ArrayList<ExampleSong> croatianList;

    private ExampleAdapter mAdapter;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /* Main activity toolbar settings for search icon */
        mainToolbar = findViewById(R.id.mainToolbar);
        setSupportActionBar(mainToolbar);
        //Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);đ
        /* Set up the drawer menu */
        drawerLayout = findViewById(R.id.drawerLayout);
        navigationView = findViewById(R.id.navigationView);
        navigationView.setNavigationItemSelectedListener(this);

        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, mainToolbar, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
        actionBarDrawerToggle.syncState();

        navigationView.setCheckedItem(R.id.langSerbian);

        croatianList = createList("hrvatske.txt");
        serbianList = createList("srpske.txt");
        slovakList = createList("slovensky.txt");

        setRecyclerView(serbianList);
    }

    // Sets up the search icon
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.getFilter().filter(newText);
                return false;
            }
        });
        return true;
    }

    // Load the RecyclerView with the list of songs
    public void setRecyclerView(final ArrayList<ExampleSong> exampleList){
        RecyclerView mRecycleView = findViewById(R.id.recyclerView);
        mRecycleView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mAdapter = new ExampleAdapter(exampleList);

        mRecycleView.setLayoutManager(mLayoutManager);
        mRecycleView.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new ExampleAdapter.OnItemClickListener() {
            @Override
            public void OnItemClick(int position) {
                // open new intent
                Bundle bundle = new Bundle();
                bundle.putParcelable("Example item",  exampleList.get(position));
                Intent intent = new Intent(MainActivity.this, SongOpened.class);
                intent.putExtras(bundle);
                //intent.putExtra("Example item", exampleList.get(position));
                startActivity(intent);
            }
        });
    }

    // Read songs from file and return in array list
    public ArrayList<ExampleSong> createList(String fileName) {
        BufferedReader reader = null;
        try {
            ArrayList<ExampleSong> exampleList = new ArrayList<>();
            reader = new BufferedReader(
                    new InputStreamReader(getAssets().open(fileName)));

            // do reading, usually loop until end of file reading
            String mLine;
            while ((mLine = reader.readLine()) != null) {
                int begin = mLine.indexOf(" ");
                int end = mLine.length();

                String num = mLine.substring(0,begin);
                String title = mLine.substring(begin+1, end);

                exampleList.add(new ExampleSong(title,num));
            }
            return exampleList;
        } catch (IOException e) {
            //log the exception
            return null;
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    //log the exception
                }
            }
        }

    }

    // when navigation item is pressed
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.langSerbian:
                currentItem = 0;
                setRecyclerView(serbianList);
                drawerLayout.closeDrawer(GravityCompat.START);
                break;
            case R.id.langSlovak:
                currentItem = 1;
                setRecyclerView(slovakList);
                drawerLayout.closeDrawer(GravityCompat.START);
                break;
            case R.id.langCroatian:
                currentItem = 2;
                setRecyclerView(croatianList);
                drawerLayout.closeDrawer(GravityCompat.START);
                break;
            case R.id.ourWebsite:
                Uri uri = Uri.parse(WEBSITE);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                break;
            case R.id.aboutUs:
                startActivity(new Intent(MainActivity.this, AboutUs.class));
                break;
            case R.id.contactUs:
                intent = new Intent(Intent.ACTION_SEND);
                intent.setType("plain/text");
                intent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[] {EMAIL});
                intent.putExtra(android.content.Intent.EXTRA_SUBJECT,"");
                intent.putExtra(android.content.Intent.EXTRA_TEXT, "");
                startActivity(Intent.createChooser(intent,"Send"));
                break;
            case R.id.updateApp:
                uri = Uri.parse(UPDATE);
                intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)){
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
}
