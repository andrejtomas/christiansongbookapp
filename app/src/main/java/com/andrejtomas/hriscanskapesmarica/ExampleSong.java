package com.andrejtomas.hriscanskapesmarica;

import android.os.Parcel;
import android.os.Parcelable;

public class ExampleSong implements Parcelable {

    private String songTitle;
    private String songNumber;

    public ExampleSong(String songTitle, String songNumber) {
        this.songTitle = songTitle;
        this.songNumber = songNumber;
    }

    protected ExampleSong(Parcel in) {
        songTitle = in.readString();
        songNumber = in.readString();
    }

    public static final Creator<ExampleSong> CREATOR = new Creator<ExampleSong>() {
        @Override
        public ExampleSong createFromParcel(Parcel in) {
            return new ExampleSong(in);
        }

        @Override
        public ExampleSong[] newArray(int size) {
            return new ExampleSong[size];
        }
    };

    public String getSongTitle() {
        return songTitle;
    }

    public String getSongNumber() {
        return songNumber;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(songTitle);
        dest.writeString(songNumber);
    }
}